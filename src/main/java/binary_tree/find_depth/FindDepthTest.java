package binary_tree.find_depth;

import binary_tree.Tree;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FindDepthTest {


    @Test
    public void test1() {
        Tree<Integer> tree = new Tree<Integer>(0);

        Assert.assertEquals(FindDepth.getDepth(tree), 1);
    }

    @Test
    public void test2() {
        Tree<Integer> tree = new Tree<Integer>(0);
        tree.left = new Tree<Integer>(-1);

        Assert.assertEquals(FindDepth.getDepth(tree), 2);
    }

    @Test
    public void test3() {
        Tree<Integer> tree = new Tree<Integer>(0);
        tree.left = new Tree<Integer>(-1);
        tree.left.left = new Tree<Integer>(-2);

        Assert.assertEquals(FindDepth.getDepth(tree), 3);
    }

    @Test
    public void test4() {
        Tree<Integer> tree = new Tree<Integer>(0);
        tree.left = new Tree<Integer>(-3);
        tree.left.right = new Tree<Integer>(-2);
        tree.left.right.right = new Tree<Integer>(-1);
        tree.left.left = new Tree<Integer>(-4);
        tree.right = new Tree<Integer>(2);

        Assert.assertEquals(FindDepth.getDepth(tree), 4);
    }

}
