package binary_tree.find_depth;

import binary_tree.Tree;

public class FindDepth {

    public static int getDepth(Tree<Integer> t) {
        if (t != null) {
            if (t.value != null) {
                if (t.left == null && t.right == null) {
                    return 1;
                }
                int leftDepth = 1;
                int rightDepth = 1;
                if (t.left != null) {
                    leftDepth += getDepth(t.left);
                }
                if (t.right != null) {
                    rightDepth += getDepth(t.right);
                }
                return Math.max(leftDepth, rightDepth);
            }
            return 1;
        }
        return 1;
    }

}
