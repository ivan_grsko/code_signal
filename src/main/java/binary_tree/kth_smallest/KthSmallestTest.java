package binary_tree.kth_smallest;

import binary_tree.Tree;
import org.testng.Assert;
import org.testng.annotations.Test;

public class KthSmallestTest {

    @Test
    public void test1() {
        Tree<Integer> tree = new Tree<Integer>(0);
        int kth = new KthSmallest().kthSmallestInBST(tree, 1);

        Assert.assertEquals(kth, 0);
    }

    @Test
    public void test2() {
        Tree<Integer> tree = new Tree<Integer>(0);
        tree.left = new Tree<Integer>(-1);
        int kth = new KthSmallest().kthSmallestInBST(tree, 1);

        Assert.assertEquals(kth, -1);
    }

    @Test
    public void test3() {
        Tree<Integer> tree = new Tree<Integer>(0);
        tree.left = new Tree<Integer>(-1);
        tree.left.left = new Tree<Integer>(-2);
        int kth = new KthSmallest().kthSmallestInBST(tree, 1);

        Assert.assertEquals(kth, -2);
    }

    @Test
    public void test4() {
        Tree<Integer> tree = new Tree<Integer>(0);
        tree.left = new Tree<Integer>(-3);
        tree.left.right = new Tree<Integer>(-2);
        tree.left.right.right = new Tree<Integer>(-1);
        tree.left.left = new Tree<Integer>(-4);
        tree.right = new Tree<Integer>(2);

        int kth = new KthSmallest().kthSmallestInBST(tree, 1);
        Assert.assertEquals(kth, -4);

        kth = new KthSmallest().kthSmallestInBST(tree, 4);
        Assert.assertEquals(kth, -1);

        kth = new KthSmallest().kthSmallestInBST(tree, 6);
        Assert.assertEquals(kth, 2);
    }
}
