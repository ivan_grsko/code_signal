package binary_tree.kth_smallest;

import binary_tree.Tree;

import java.util.ArrayList;
import java.util.List;

public class KthSmallest {

    int kthSmallestInBST(Tree<Integer> t, int k) {
        List<Integer> list = treeToArray(t);
        return list.get(k-1);
    }

    private List<Integer> treeToArray(Tree<Integer> t) {
        List<Integer> list = new ArrayList<Integer>();
        if (t == null) {
            return list;
        }

        if (t.left != null) {
            list.addAll(treeToArray(t.left));
        }
        list.add(t.value);
        if (t.right != null) {
            list.addAll(treeToArray(t.right));
        }

        return list;
    }

}
