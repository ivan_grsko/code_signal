package binary_tree.symmetry;

public class TreeSymmetry<T> {

    // region DO NOT CHANGE
    TreeSymmetry(T x) {
        value = x;
    }

    T value;
    TreeSymmetry<T> left;
    TreeSymmetry<T> right;
    // endregion

    // TODO: Implement
    boolean isTreeSymmetric(TreeSymmetry<Integer> t) {
        if (t == null)
            return true;

        // branches nulls comparison
        if (t.left == null && t.right == null)
            return true;
        if (t.left == null && t.right != null)
            return false;
        if (t.left != null && t.right == null)
            return false;

        return compareTwoNodes(t.left, t.right);
    }

    private boolean compareTwoNodes(TreeSymmetry<Integer> left, TreeSymmetry<Integer> right) {
        if (left.value != null && right.value != null) {
            if (!left.value.equals(right.value))
                return false;
        }
        if ((left.value == null && right.value != null) || (left.value != null && right.value == null))
            return false;


        // outer branches nulls comparison
        boolean outerComparison = true;
        if ((left.left != null && right.right == null) || (left.left == null && right.right != null))
            return false;
        if (left.left != null && right.right != null)
            outerComparison = compareTwoNodes(left.left, right.right);

        // inner branches nulls comparison
        boolean innerComparison = true;
        if ((left.right != null && right.left == null) || (left.right == null && right.left != null))
            return false;
        if (left.right != null && right.left != null)
            innerComparison = compareTwoNodes(left.right, right.left);


        return outerComparison && innerComparison;
    }

    // good solution below...




































    /*
    boolean isTreeSymmetric(TreeSymmetry<Integer> t) {
        return isMirror(t, t);
    }

    boolean isMirror(TreeSymmetry t1, TreeSymmetry t2) {
        if (t1 == null && t2 == null) return true;
        if (t1 == null || t2 == null) return false;
        return (t1.value).equals(t2.value)
                && isMirror(t1.right, t2.left)
                && isMirror(t1.left, t2.right);
    }*/
}
