package binary_tree.symmetry;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TreeSymmetryTest {

    /*@Test
    public void test0_00() {
    Integer a = 374;
    Integer b = 374;
    if (a == b)
        System.out.println("equal");
    else
        System.out.println("not equal");

        Assert.assertEquals(a == b, true);
    }*/

    @Test
    public void test0() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1

        Assert.assertEquals(tree.isTreeSymmetric(null), true);
    }

    @Test
    public void test0_0() {
        TreeSymmetry tree = new TreeSymmetry(null);    // root 1

        Assert.assertEquals(tree.isTreeSymmetric(tree), true);
    }

    @Test
    public void test1() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1

        Assert.assertEquals(tree.isTreeSymmetric(tree), true);
    }

    @Test
    public void test2() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2

        Assert.assertEquals(tree.isTreeSymmetric(tree), false);
    }

    @Test
    public void test3() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(2);    // right 2

        Assert.assertEquals(tree.isTreeSymmetric(tree), true);
    }

    @Test
    public void test3_1() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(null);    // left null
        tree.right = new TreeSymmetry(null);    // right null

        Assert.assertEquals(tree.isTreeSymmetric(tree), true);
    }

    @Test
    public void test4() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(3);    // right 3

        Assert.assertEquals(tree.isTreeSymmetric(tree), false);
    }

    @Test
    public void test4_1() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(null);    // right null

        Assert.assertEquals(tree.isTreeSymmetric(tree), false);
    }

    @Test(description = "Positive: 2nd level outer")
    public void test5() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(2);    // right 2
        tree.left.left = new TreeSymmetry(0); // left-left 0
        tree.right.right = new TreeSymmetry(0); // right-right 0

        Assert.assertEquals(tree.isTreeSymmetric(tree), true);
    }

    @Test(description = "Negative: asymmetry on 2nd level")
    public void test6() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(2);    // right 2
        tree.left.left = new TreeSymmetry(0); // left-left 0
        tree.right.left = new TreeSymmetry(0); // right-left 0

        Assert.assertEquals(tree.isTreeSymmetric(tree), false);
    }

    @Test(description = "Negative: asymmetry on 2nd level")
    public void test7() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(2);    // right 2
        tree.left.right = new TreeSymmetry(0); // left-right 0
        tree.right.right = new TreeSymmetry(0); // right-right 0

        Assert.assertEquals(tree.isTreeSymmetric(tree), false);
    }

    @Test(description = "Positive: 2nd level with null values")
    public void test8() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(2);    // right 2
        tree.left.left = new TreeSymmetry(null); // left-left null
        tree.right.right = new TreeSymmetry(null); // right-right null

        Assert.assertEquals(tree.isTreeSymmetric(tree), true);
    }

    @Test(description = "Negative: diff on 2nd level (null value in tree)")
    public void test9() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(2);    // right 2
        tree.left.left = new TreeSymmetry(null); // left-left null
        tree.right.right = new TreeSymmetry(0); // right-right 0

        Assert.assertEquals(tree.isTreeSymmetric(tree), false);
    }

    @Test(description = "Negative: diff on 2nd level (partially)")
    public void test10() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(2);    // right 2
        tree.left.left = new TreeSymmetry(3); // left-left 3
        tree.right.right = new TreeSymmetry(4); // right-right 4

        Assert.assertEquals(tree.isTreeSymmetric(tree), false);
    }

    @Test(description = "Positive: full 2nd level")
    public void test11() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(2);    // right 2
        tree.left.left = new TreeSymmetry(3); // left-left 3
        tree.right.right = new TreeSymmetry(3); // right-right 3
        tree.left.right = new TreeSymmetry(4); // left-left 3
        tree.right.left = new TreeSymmetry(4); // right-right 4

        Assert.assertEquals(tree.isTreeSymmetric(tree), true);
    }

    @Test(description = "Negative: diff on 2nd level")
    public void test12() {
        TreeSymmetry tree = new TreeSymmetry(1);    // root 1
        tree.left = new TreeSymmetry(2);    // left 2
        tree.right = new TreeSymmetry(2);    // right 2
        tree.left.left = new TreeSymmetry(3); // left-left 3
        tree.right.right = new TreeSymmetry(3); // right-right 3
        tree.left.right = new TreeSymmetry(4); // left-left 3
        tree.right.left = new TreeSymmetry(5); // right-right 5

        Assert.assertEquals(tree.isTreeSymmetric(tree), false);
    }

    @Test(description = "")
    public void test13() {
        TreeSymmetry tree = new TreeSymmetry(-191);    // root -191

        tree.left = new TreeSymmetry(374);    // left 374
        tree.left.left = new TreeSymmetry(-361); // left-left -361
        tree.left.left.left = new TreeSymmetry(-771); // left-left-left -771
        tree.left.left.left.left = null;
        tree.left.left.left.right = new TreeSymmetry(-379);
        tree.left.left.left.right.left = new TreeSymmetry(-154);
        tree.left.left.left.right.right = new TreeSymmetry(-699);

        tree.left.left.right = new TreeSymmetry(159);
        tree.left.left.right.left = new TreeSymmetry(-900);
        tree.left.left.right.left.left = new TreeSymmetry(305);
        tree.left.left.right.left.right = new TreeSymmetry(-486);

        tree.left.left.right.right = new TreeSymmetry(200);
        tree.left.left.right.right.left = new TreeSymmetry(-699);
        tree.left.left.right.right.right = new TreeSymmetry(470);


        tree.left.right = null;

        //-----------------

        tree.right = new TreeSymmetry(374);
        tree.right.right = new TreeSymmetry(-361);
        tree.right.right.right = new TreeSymmetry(-771);
        tree.right.right.right.right = null;
        tree.right.right.right.left = new TreeSymmetry(-379);
        tree.right.right.right.left.right = new TreeSymmetry(-154);
        tree.right.right.right.left.left = new TreeSymmetry(-699);

        tree.right.right.left = new TreeSymmetry(159);
        tree.right.right.left.right = new TreeSymmetry(-900);
        tree.right.right.left.right.right = new TreeSymmetry(305);
        tree.right.right.left.right.left = new TreeSymmetry(-486);

        tree.right.right.left.left = new TreeSymmetry(200);
        tree.right.right.left.left.right = new TreeSymmetry(-699);
        tree.right.right.left.left.left = new TreeSymmetry(470);

        tree.right.left = null;

        Assert.assertEquals(tree.isTreeSymmetric(tree), true);
    }

    /*
    * t: {
    "value": -191,
    "left": {
        "value": 374,
        "left": {
            "value": -361,
            "left": {
                "value": -771,
                "left": null,
                "right": {
                    "value": -379,
                    "left": {
                        "value": -154,
                        "left": null,
                        "right": null
                    },
                    "right": {
                        "value": -699,
                        "left": null,
                        "right": null
                    }
                }
            },
            "right": {
                "value": 159,
                "left": {
                    "value": -900,
                    "left": {
                        "value": 305,
                        "left": null,
                        "right": null
                    },
                    "right": {
                        "value": -486,
                        "left": null,
                        "right": null
                    }
                },
                "right": {
                    "value": 200,
                    "left": {
                        "value": -699,
                        "left": null,
                        "right": null
                    },
                    "right": {
                        "value": 470,
                        "left": null,
                        "right": null
                    }
                }
            }
        },
        "right": null
    },
    "right": {
        "value": 374,
        "left": null,
        "right": {
            "value": -361,
            "left": {
                "value": 159,
                "left": {
                    "value": 200,
                    "left": {
                        "value": 470,
                        "left": null,
                        "right": null
                    },
                    "right": {
                        "value": -699,
                        "left": null,
                        "right": null
                    }
                },
                "right": {
                    "value": -900,
                    "left": {
                        "value": -486,
                        "left": null,
                        "right": null
                    },
                    "right": {
                        "value": 305,
                        "left": null,
                        "right": null
                    }
                }
            },
            "right": {
                "value": -771,
                "left": {
                    "value": -379,
                    "left": {
                        "value": -699,
                        "left": null,
                        "right": null
                    },
                    "right": {
                        "value": -154,
                        "left": null,
                        "right": null
                    }
                },
                "right": null
            }
        }
    }
}
    * */

}
