package binary_tree.find_profession;

public class FindProfession {

    public static String findProfession(int level, int pos) {

        return boolToProf(getChild(true /*"Engineer"*/, level, pos));
        //return "Engineer"; // "Doctor"
    }

    private static boolean getChild(boolean parentProfession, int level, int pos) {
        double totalPositionsCount = Math.pow(2, level - 1);
        double halfOfPositionsOnLevel = (totalPositionsCount / 2) + 0.1;
        if (halfOfPositionsOnLevel < 1.0) {
            return parentProfession;
        }
        double floor = Math.floor(halfOfPositionsOnLevel);
        if (halfOfPositionsOnLevel > pos*1.0) {
            return getChild(parentProfession, level - 1, pos);
        } else {
            int halfOfPositionsOnLevelInt = (int) halfOfPositionsOnLevel;
            return getChild(!parentProfession, level - 1, pos - halfOfPositionsOnLevelInt);
        }
        //return true;
    }

    private static String boolToProf(boolean trueEng) {
        return trueEng ? "Engineer" : "Doctor";
    }

    // Better solution below
























































    /*
    String findProfession(int level, int pos) {
        boolean inverted = false;

        while (level > 1) {
            if (pos % 2 == 0) {
                inverted = !inverted;
            }

            pos = (pos + 1) / 2;
            level--;
        }

        return inverted? "Doctor" : "Engineer";
    }
    */
}
