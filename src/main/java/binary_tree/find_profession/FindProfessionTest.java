package binary_tree.find_profession;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FindProfessionTest {

    // 1 ≤ level ≤ 30
    // 1 ≤ pos ≤ 2(level - 1)
    // Return Engineer or Doctor

    @Test
    public void test1() {
        Assert.assertEquals(FindProfession.findProfession(1, 1), "Engineer");
    }

    @Test
    public void test2() {
        Assert.assertEquals(FindProfession.findProfession(2, 1), "Engineer");
    }

    @Test
    public void test3() {
        Assert.assertEquals(FindProfession.findProfession(2, 2), "Doctor");
    }

    @Test
    public void test4() {
        Assert.assertEquals(FindProfession.findProfession(3, 1), "Engineer");
    }

}