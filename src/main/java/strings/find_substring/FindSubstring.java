package strings.find_substring;

public class FindSubstring {

    /** Find position of x in s // SOLUTION THAT IS NOT FAST ENOUGH
     * @param x A string containing only uppercase or lowercase English; 1 ≤ x.length ≤ 10^6
     * @param s A string containing only uppercase or lowercase English
     */
    // testTimingWorstCaseAtTheEnd() : 10runs AVG = // didn't finish within the time-out 10000
    /*int strstr(String s, String x) {
        char[] sChars = s.toCharArray();
        char[] xChars = x.toCharArray();

        for (int i = 0; i < sChars.length - xChars.length + 1; i++) {
            if (sChars[i] == xChars[0]) {
                if (xChars.length == 1) {
                    return i;
                } else {
                    for (int j = 1; j < xChars.length; j++) {
                        if (sChars[i+j] != xChars[j]) {
                            break;
                        } else {
                            if (j == xChars.length - 1) {
                                return i;
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }   //*/

    /** Find position of x in s // BETTER SOLUTION
     * @param x A string containing only uppercase or lowercase English; 1 ≤ x.length ≤ 10^6
     * @param s A string containing only uppercase or lowercase English
     */
    // testTimingWorstCaseAtTheEnd() : 10runs AVG = 0.2936s.
    int strstr(String s, String x) {
        char[] sChars = s.toCharArray();
        char[] xChars = x.toCharArray();

        for (int i = 0; i < sChars.length - xChars.length + 1; i++) {
            if (sChars[i] == xChars[0]) {
                if (xChars.length == 1) {
                    return i;
                } else {
                    for (int j = xChars.length - 1; j > 0; j--) {
                        if (sChars[i+j] != xChars[j]) {
                            break;
                        } else {
                            if (j == 1) {
                                return i;
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }   //*/

    /** Find position of x in s // BEST SOLUTION????
     * @param x A string containing only uppercase or lowercase English; 1 ≤ x.length ≤ 10^6
     * @param s A string containing only uppercase or lowercase English
     */
    /*int strstr(String s, String x) {
        char[] sChars = s.toCharArray();
        char[] xChars = x.toCharArray();

        for (int i = 0; i < sChars.length - xChars.length + 1; i++) {
            if (sChars[i] == xChars[0]) {
                if (xChars.length == 1) {
                    return i;
                } else {
                    for (int t = xChars.length - 1; t > 0; t--) {
                        int b = i + 1 + (xChars.length - 1 - t);
                        if (sChars[i+t] != xChars[t]) {
                            break;
                        } else {
                            if (t == 1) {
                                return i;
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }   //*/



    // E.K. solution
    /*public int strstr(String in, String find){
        if (in.length() < find.length())
            return -1;

        int i = 0;
        while(i < in.length() - find.length()){
            if (in.substring(i, i + find.length()).equals(find)){
                return i;
            }
            i++;
        }
        return -1;
    }   //*/
}
