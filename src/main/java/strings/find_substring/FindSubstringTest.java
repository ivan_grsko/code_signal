package strings.find_substring;

import org.testng.Assert;
import org.testng.annotations.Test;

public class FindSubstringTest {

    FindSubstring findSubstring = new FindSubstring();

    @Test
    public void testSimplePositive() {
        String s = "CodefightsIsAwesome";
        String x = "IsA";

        Assert.assertEquals(findSubstring.strstr(s, x), 10);
    }

    @Test
    public void testSimpleNegative() {
        String s = "CodefightsIsAwesome";
        String x = "IA";

        Assert.assertEquals(findSubstring.strstr(s, x), -1);
    }

    @Test
    public void testMatchFoundAtTheEndOfStringPositive() {
        String s = "CodefightsIsAwesome";
        String x = "me";

        Assert.assertEquals(findSubstring.strstr(s, x), 17);
    }


    @Test(timeOut = 10000)
    public void testTimingWorstCaseAtTheEnd() {
        int aLengthInMain = 100000000;
        String s = "A".repeat(aLengthInMain) + "B";
        int aLengthInSubstr = 50000000;
        String x = "A".repeat(aLengthInSubstr) + "B";

        long start = System.currentTimeMillis();
        int strstr = 0;
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        long delta = System.currentTimeMillis() - start;
        System.out.println("testTimingWorstCaseAtTheEnd() time (AVG of 10 runs) = " + (delta / 10000.0) + "s.");
        Assert.assertEquals(strstr, aLengthInMain - aLengthInSubstr);
    }

    @Test(timeOut = 10000)
    public void testTimingWorstCaseAtTheMiddle() {
        int aLengthInMain = 50000000;
        String s = "A".repeat(aLengthInMain) + "B".repeat(aLengthInMain);
        int aLengthInSubstr = 25000000;
        String x = "A".repeat(aLengthInSubstr) + "B".repeat(aLengthInSubstr);

        long start = System.currentTimeMillis();
        int strstr = 0;
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        strstr = findSubstring.strstr(s, x);
        long delta = System.currentTimeMillis() - start;
        System.out.println("testTimingWorstCaseAtTheEnd() time (AVG of 10 runs) = " + (delta / 10000.0) + "s.");
        Assert.assertEquals(strstr, aLengthInMain - aLengthInSubstr);
    }

    @Test
    public void testJavaHeapSpace() {
        int aLengthInMain = 1000000000;
        String s = "A".repeat(aLengthInMain) + "B";
        int aLengthInSubstr = 100000;
        String x = "A".repeat(aLengthInSubstr) + "B";

        Assert.assertEquals(findSubstring.strstr(s, x), aLengthInMain - aLengthInSubstr);
    }

    @Test
    public void testRequestedArraySizeExceedsVMLimit() {
        int aLengthInMain = Integer.MAX_VALUE;
        String s = "A".repeat(aLengthInMain) + "B";
        int aLengthInSubstr = (int)(Math.sqrt(Integer.MAX_VALUE * 1.0));
        String x = "A".repeat(aLengthInSubstr) + "B";

        Assert.assertEquals(findSubstring.strstr(s, x), aLengthInMain - aLengthInSubstr);
    }
}
