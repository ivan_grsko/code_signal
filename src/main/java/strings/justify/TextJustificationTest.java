package strings.justify;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TextJustificationTest {
    TextJustification textJustification = new TextJustification();

    @Test
    public void testHelloWorld() {
        String[] inputArray = {"Hello", "world"};
        String[] expectedArray = {"Hello world"};
        int l = 11;

        String[] actualArray = textJustification.textJustification(inputArray, l);

        checkArray(actualArray, expectedArray);
    }

    @Test
    public void testHelloWorldLonger() {
        String[] inputArray = {"Hello", "world"};
        String[] expectedArray = {"Hello world  "};
        int l = 13;

        String[] actualArray = textJustification.textJustification(inputArray, l);

        checkArray(actualArray, expectedArray);
    }

    @Test
    public void testSingleLetters() {
        String[] inputArray = {"H", "e", "l", "l", "o", "w", "o", "r", "l", "d"};
        String[] expectedArray = {
                "H e l l",
                "o w o r",
                "l d    "};
        int l = 7;

        String[] actualArray = textJustification.textJustification(inputArray, l);

        checkArray(actualArray, expectedArray);
    }

    @Test
    public void testLongSpaces() {
        String[] inputArray = {"Hell", "o", "world"};
        String[] expectedArray = {
                "Hell      o",
                "world      "};
        int l = 11;

        String[] actualArray = textJustification.textJustification(inputArray, l);

        checkArray(actualArray, expectedArray);
    }

    @Test
    public void testNotEqualSpaces() {
        String[] inputArray = {"ab", "cd", "ef", "gh", "longworld"};
        String[] expectedArray = {
                "ab  cd  ef gh",
                "longworld    "};
        int l = 13;

        String[] actualArray = textJustification.textJustification(inputArray, l);

        checkArray(actualArray, expectedArray);
    }

    @Test
    public void testNotEqualSpaces2() {
        String[] inputArray = {
                "This", "is",
                "an",
                "example",
                "of",
                "text",
                "justification."};
        String[] expectedArray = {
                "This    is    an",
                "example  of text",
                "justification.  "};
        int l = 16;

        String[] actualArray = textJustification.textJustification(inputArray, l);

        checkArray(actualArray, expectedArray);
    }

    @Test
    public void testNotEqualSpaces3() {
        String[] inputArray = {
                "Looks",
                "like",
                "it",
                "can",
                "be",
                "a",
                "tricky",
                "test"};
        String[] expectedArray = {
                "Looks  like  it  can be a",
                "tricky test              "};
        int l = 25;

        String[] actualArray = textJustification.textJustification(inputArray, l);

        checkArray(actualArray, expectedArray);
    }

    @Test
    public void testTwoWords() {
        String[] inputArray = {
                "Two",
                "words."};
        String[] expectedArray = {
                "Two      ",
                "words.   "};
        int l = 9;

        String[] actualArray = textJustification.textJustification(inputArray, l);

        checkArray(actualArray, expectedArray);
    }


    private void checkArray(String[] actualArray, String[] expectedArray) {
        String failMessage = "";
        if (actualArray.length != expectedArray.length) {
            failMessage += " * actualArray.length = " + actualArray.length + " NOT EQUAL to expectedArray.length = " + expectedArray.length + "\n";
        }

        for (int i = 0; i < Math.min(expectedArray.length, actualArray.length); i++) {
            if (actualArray[i] == null) {
                failMessage += " * actualArray[" + i + "] is NULL (stop comparison)\n";
                break;
            }
            if ("".equals(actualArray[i])) {
                failMessage += " * actualArray[" + i + "] is empty string (stop comparison)\n";
                break;
            }
            if (!expectedArray[i].equals(actualArray[i])) {
                failMessage += " * actualArray[" + i + "] = '" + actualArray[i] +
                        "' NOT EQUAL to expectedArray[" + i +"] = '" + expectedArray[i] + "' (stop comparison)\n";
                break;
            }
        }

        if (!"".equals(failMessage)) {
            Assert.fail("ERROR:\n" + failMessage);
        }
    }
}
