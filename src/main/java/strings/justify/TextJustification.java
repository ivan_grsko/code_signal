package strings.justify;

import java.util.ArrayList;
import java.util.List;

public class TextJustification {


    // region first solution
    /**
     * @param words - 1 ≤ words.length ≤ 150 AND 0 ≤ words[i].length ≤ l - Hm... Sure words[i].length could be ZERO???
     * @param l - 1 ≤ l ≤ 60
     * @return
     */
    /*String[] textJustification(String[] words, int l) {
        List<SingleLineWords> dividedByLines = new ArrayList<>();
        List<String> singleLine = new ArrayList<>();
        int lineLengthWithSingleSpacesCurrent = 0;
        for (int i = 0; i < words.length; i++) {
            String wordCurrent = words[i];
            lineLengthWithSingleSpacesCurrent += wordCurrent.length() + 1;
            singleLine.add(wordCurrent);
            if (i < words.length - 1 && lineLengthWithSingleSpacesCurrent + words[i+1].length() <= l) {
                continue;
            } else {
                // save line (one line is compiled)
                SingleLineWords singleLineWords =
                        new SingleLineWords()
                                .setLineWords(new ArrayList<>(singleLine))
                                .setTotalLettersCount(lineLengthWithSingleSpacesCurrent - singleLine.size());
                dividedByLines.add(singleLineWords);

                // null-ing for next line compilation
                lineLengthWithSingleSpacesCurrent = 0;
                singleLine = new ArrayList<>();
            }
        }
        List<String> justified = new ArrayList<>();
        for (int i = 0; i < dividedByLines.size() - 1; i++) {
            justified.add(dividedByLines.get(i).getCompiledLineOfLength(l));
        }
        justified.add(dividedByLines.get(dividedByLines.size() - 1).getCompiledLastLineOfLength(l));
        return justified.stream().toArray(String[]::new);
    }

    class SingleLineWords {
        List<String> lineWords = new ArrayList<>();
        int totalLettersCount = 0;

        public SingleLineWords setLineWords(List<String> lineWords) {
            this.lineWords = lineWords;
            return this;
        }

        public SingleLineWords setTotalLettersCount(int totalLettersCount) {
            this.totalLettersCount = totalLettersCount;
            return this;
        }

        public String getCompiledLineOfLength(int len) {
            int wordsCount = lineWords.size();
            if (wordsCount == 1) {
                String singleWord = lineWords.get(0);
                return singleWord + " ".repeat(len - singleWord.length());
            }
            int totalSpacesCount = len - totalLettersCount;
            double spacesBetweenWords = totalSpacesCount / ((wordsCount - 1) * 1.0);
            String justified = "";
            for (int i = 0; i < lineWords.size(); i++) {
                justified += lineWords.get(i);
                if (i < lineWords.size() - 1) {
                    int shortSpacesCount = (int) Math.floor(spacesBetweenWords);
                    double fractionOfSpace = spacesBetweenWords - shortSpacesCount * 1.0;
                    if (fractionOfSpace < 0.00000001) {
                        justified += " ".repeat(shortSpacesCount);
                    } else {
                        if (i * 1.0 < (fractionOfSpace * (wordsCount - 1)) - 0.00000001) {
                            justified += " ".repeat(shortSpacesCount + 1);
                        } else {
                            justified += " ".repeat(shortSpacesCount);
                        }
                    }
                }
            }
            return justified;
        }

        public String getCompiledLastLineOfLength(int l) {
            String line = String.join(" ", lineWords);
            line += " ".repeat(l - line.length());
            return line;
        }
    }   //*/
    // endregion


    // region 2nd version
    public String[] textJustification(String[] words, int l) {
        List<List<String>> wordsInLines = new ArrayList<>();

        wordsInLines = divideWordsByLines(words, l);

        List<String> justified = new ArrayList<>();
        // justify lines (except last one)
        int indexLastLine = wordsInLines.size() - 1;
        for (int i = 0; i < indexLastLine; i++) {
            justified.add(justifyLineOfWords(wordsInLines.get(i), l));
        }
        // align left last line
        justified.add(alignLeftLineOfWords(wordsInLines.get(indexLastLine), l));
        return justified.stream().toArray(String[]::new);
    }



    private List<List<String>> divideWordsByLines(String[] words, int l) {
        List<List<String>> wordsInLines = new ArrayList<>();
        List<String> lineOfWords = new ArrayList<>();
        int lineLengthWithSingleSpaces = 0;

        for (int i = 0; i < words.length; i++) {
            lineOfWords.add(words[i]);
            lineLengthWithSingleSpaces += words[i].length();

            if (i < words.length - 1    // there is a next word to check
                    && words[i+1].length() + 1 + lineLengthWithSingleSpaces <= l) {  // & it still fits the line length
                lineLengthWithSingleSpaces += 1;
            } else {    // the next word does not exist or does not fit the line length
                wordsInLines.add(lineOfWords);      // save line of words
                lineOfWords = new ArrayList<>();    // preparing to collect word for the next line
                lineLengthWithSingleSpaces = 0;
            }
        }

        return wordsInLines;
    }

    private String justifyLineOfWords(List<String> words, int l) {
        if (words.size() == 1) {
            return alignLeftLineOfWords(words, l);
        }
        int totalLettersInLine = String.join("", words).length();
        double spacesCountWithFraction = (l - totalLettersInLine) * 1.0 / (words.size() - 1.0);
        String justified = "";
        for (int i = 0; i < words.size(); i ++) {
            justified += words.get(i);
            if (i < words.size() - 1) {
                // add spaces
                int evenSpacesCount = (int) Math.floor(spacesCountWithFraction);
                justified += " ".repeat(evenSpacesCount);
                if (i < (int)((spacesCountWithFraction - evenSpacesCount*1.0) * (words.size() - 1.0) + 0.1)) {
                    justified += " ";
                }
            }
        }
        return justified;
    }

    private String alignLeftLineOfWords(List<String> words, int l) {
        String joinedWords = String.join(" ", words);
        return joinedWords + " ".repeat(l - joinedWords.length());
    }

    // endregion

}
