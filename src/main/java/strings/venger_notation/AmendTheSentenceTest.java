package strings.venger_notation;

import org.testng.Assert;
import org.testng.annotations.Test;

public class AmendTheSentenceTest {
    AmendTheSentence amend = new AmendTheSentence();

    @Test
    public void testBasic() {
        String input = "HelloWorld";
        String resultExpected = "hello world";

        String result = amend.amendTheSentence(input);
        Assert.assertEquals(result, resultExpected);
    }

    @Test
    public void testSingleLetters() {
        String input = "ABCDEFGH";
        String resultExpected = "a b c d e f g h";

        String result = amend.amendTheSentence(input);
        Assert.assertEquals(result, resultExpected);
    }

    @Test
    public void testAZaz() {
        String input = "AZaz";
        String resultExpected = "a zaz";

        String result = amend.amendTheSentence(input);
        Assert.assertEquals(result, resultExpected);
    }

    @Test
    public void testFromLowerCase() {
        String input = "vwvwVvvJkkjK";
        String resultExpected = "vwvw vvv jkkj k";

        String result = amend.amendTheSentence(input);
        Assert.assertEquals(result, resultExpected);
    }
}
