package strings.venger_notation;

public class AmendTheSentence {

    String amendTheSentence(String s) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char c : s.toCharArray()) {
            if (65 <= c && c <= 90) {
                stringBuilder.append(" " + String.valueOf(c).toLowerCase());
                continue;
            }
            stringBuilder.append(c);
        }
        String result = stringBuilder.toString();
        return result.substring(0,1).equals(" ") ? result.substring(1) : result;
    }
}




/* BEST SOLUTION

String amendTheSentence(String s) {
    String[] split = s.split("(?=[A-Z])");
    return String.join(" ", split).toLowerCase();
}

*/