package strings.clussify;

public class ClassifyStrings {

    /** good, bad (If a string has 3 consecutive vowels or 5 consecutive consonants, or both) or
     * mixed (the string "?aa" can be bad if ? is a vowel or good if it is a consonant)
     * @param s - A string that can contain only lowercase English letters and the character ? AND 1 ≤ s.length ≤ 50
     * @return good, bad or mixed
     */
    /*String classifyStrings(String s) {
        int badVowelsCount = 3;
        int badConsonantsCount = 5;

        StringType currentType = StringType.GOOD;
        char[] chars = s.toCharArray();
        int consonantsCount = 0;
        int vowelsCount = 0;
        int questionSymbolCountLast = 0;
        int questionSymbolCountInSameStringType = 0;
        boolean isSingleQuestionBetweenVowelAndConsonant = false;
        boolean isCurrentVowel = true;
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == '?') {
                questionSymbolCountInSameStringType += 1;
                questionSymbolCountLast += 1;
                isSingleQuestionBetweenVowelAndConsonant = false;
            } else if (isVowel(c)) {    // VOWEL
                if (isCurrentVowel == false) {   // change of basic string type
                    questionSymbolCountInSameStringType = questionSymbolCountLast;
                    if (questionSymbolCountLast == 1 && consonantsCount == badConsonantsCount - 1) {
                        // waiting for BAD if will meet 2 vowels
                        isSingleQuestionBetweenVowelAndConsonant = true;
                    } else {
                        isSingleQuestionBetweenVowelAndConsonant = false;
                    }
                }
                questionSymbolCountLast = 0;    // stop counting last ? sequence
                consonantsCount = 0;            // no more consonants

                isCurrentVowel = true;
                vowelsCount += 1;

            } else {    // CONSONANT
                if (isCurrentVowel == true) {   // change of basic string type
                    questionSymbolCountInSameStringType = questionSymbolCountLast;
                    if (questionSymbolCountLast == 1 && vowelsCount == badVowelsCount - 1) {
                        // waiting for BAD if will meet 4 consonants
                        isSingleQuestionBetweenVowelAndConsonant = true;
                    } else {
                        isSingleQuestionBetweenVowelAndConsonant = false;
                    }
                }
                questionSymbolCountLast = 0;    // stop counting last ? sequence
                vowelsCount = 0;                // no more vowels

                isCurrentVowel = false;
                consonantsCount += 1;
            }

            if (consonantsCount >= badConsonantsCount
                    || vowelsCount >= badVowelsCount
                    || (isSingleQuestionBetweenVowelAndConsonant == true
                        && (vowelsCount == badVowelsCount - 1 || consonantsCount == badConsonantsCount - 1))) {
                return StringType.BAD.toString();
            }
            if (consonantsCount + questionSymbolCountInSameStringType >= badConsonantsCount
                    || vowelsCount + questionSymbolCountInSameStringType >= badVowelsCount) {
                currentType = StringType.MIXED;
            }
        }

        return currentType.toString();
    }

    boolean isVowel(char c) {
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        for(char vowel : vowels) {
            if (vowel == c) {
                return true;
            }
        }
        return false;
    }

    public enum StringType {
        // about priority:
        GOOD ("good"),      // to say that string is good we need to check the string from the beginning till the end
        MIXED ("mixed"),    // if we discover that string is MIXED  at some point it is no more GOOD, but we still need
                            // to check till the end of the string (it could be BAD)
        BAD ("bad");        // if we discover that string is BAD    at some point there is no reason for further checks

        private final String name;
        StringType(String s) {
            name = s;
        }
        public String toString() {
            return this.name;
        }
    }       //*/



    // SOLUTION 2

    /** good, bad (If a string has 3 consecutive vowels or 5 consecutive consonants, or both) or
     * mixed (the string "?aa" can be bad if ? is a vowel or good if it is a consonant)
     * @param s - A string that can contain only lowercase English letters and the character ? AND 1 ≤ s.length ≤ 50
     * @return good, bad or mixed
     */
    String classifyStrings(String s) {
        int badVowelsCount = 3;
        int badConsonantsCount = 5;
        StringType currentType = StringType.GOOD;

        char[] chars = s.toCharArray();
        int consonantsCount = 0;
        int consonantsMixCount = 0; // + '?'
        int vowelsCount = 0;
        int vowelsMixedCount = 0;   // + '?'
        boolean isPreviousVowel = true;
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (c == '?') {
                consonantsMixCount += 1;
                vowelsMixedCount += 1;

                if (consonantsCount == badConsonantsCount - 1) {
                    currentType = StringType.MIXED;
                    vowelsCount += 1;   // if we do not replace ? by vowel the string is BAD, so trying to go on...
                    consonantsCount = 0;
                    //isPreviousVowel = true;
                } else if (vowelsCount == badVowelsCount - 1) {
                    currentType = StringType.MIXED;
                    consonantsCount += 1;   // if we do not replace ? by consonant the string is BAD, so trying to go on...
                    vowelsCount = 0;
                    //isPreviousVowel = false;
                } else if (i < chars.length - 1 && chars[i+1] != '?') {
                    if (isVowel(chars[i+1])) { // vowel after '?'
                        consonantsCount = 1;
                        vowelsCount = 0;
                    } else {
                        vowelsCount = 1;
                        consonantsCount = 0;
                    }
                } else {
                    vowelsCount = 0;
                    consonantsCount = 0;
                }

            } else if (isVowel(c)) {    // VOWEL
                vowelsCount += 1;
                vowelsMixedCount += 1;
                consonantsCount = 0;
                consonantsMixCount = 0;

            } else {                    // CONSONANT
                consonantsCount += 1;
                consonantsMixCount += 1;
                vowelsCount = 0;
                vowelsMixedCount = 0;
            }

            if (vowelsCount == badVowelsCount || consonantsCount == badConsonantsCount) {
                return StringType.BAD.toString();
            }
            if (vowelsMixedCount == badVowelsCount || consonantsMixCount == badConsonantsCount) {
                currentType = StringType.MIXED;
            }
        }
        return currentType.toString();
    }

    boolean isVowel(char c) {
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        for(char vowel : vowels) {
            if (vowel == c) {
                return true;
            }
        }
        return false;
    }

    public enum StringType {
        // about priority:
        GOOD ("good"),      // to say that string is good we need to check the string from the beginning till the end
        MIXED ("mixed"),    // if we discover that string is MIXED  at some point it is no more GOOD, but we still need
        // to check till the end of the string (it could be BAD)
        BAD ("bad");        // if we discover that string is BAD    at some point there is no reason for further checks

        private final String name;
        StringType(String s) {
            name = s;
        }
        public String toString() {
            return this.name;
        }
    }

    // Add regex solution

}
