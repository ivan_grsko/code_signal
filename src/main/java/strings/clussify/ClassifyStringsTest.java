package strings.clussify;

import org.testng.Assert;
import org.testng.annotations.Test;

import static strings.clussify.ClassifyStrings.StringType.*;

public class ClassifyStringsTest {

    ClassifyStrings classifyStrings = new ClassifyStrings();

    // GOOD

    @Test
    public void testGoodNoQuestionSymbol() {
        String goodString = "aadferedfghionerrrr";

        Assert.assertEquals(classifyStrings.classifyStrings(goodString), GOOD.toString(), "test val: " + goodString);
    }

    @Test
    public void testGoodWithQuestionSymbol() {
        String goodString = "a?dferedfghi?nnnerrr?";

        Assert.assertEquals(classifyStrings.classifyStrings(goodString), GOOD.toString(), "test val: " + goodString);
    }


    // BAD

    @Test
    public void testBadByConsonants() {
        String badString = "aadfedfghhion";

        Assert.assertEquals(classifyStrings.classifyStrings(badString), BAD.toString(), "test val: " + badString);
    }

    @Test
    public void testBadByVowels() {
        String badString = "aadfedeoifghhion";

        Assert.assertEquals(classifyStrings.classifyStrings(badString), BAD.toString(), "test val: " + badString);
    }
    // BAD with ?
    @Test
    public void testBadWithVowelsQuestionConsonants() {
        String badString = "aadfedeo?fghhion";

        Assert.assertEquals(classifyStrings.classifyStrings(badString), BAD.toString(), "test val: " + badString);
    }

    @Test
    public void testBadWithConsonantsQuestionVowels() {
        String badString = "aadfedddd?iondd";

        Assert.assertEquals(classifyStrings.classifyStrings(badString), BAD.toString(), "test val: " + badString);
    }

    @Test
    public void testBadWithConsonantsQuestionVowelsQuestionConsonants() {
        String badString = "aadfedddd?i?nddduy";

        Assert.assertEquals(classifyStrings.classifyStrings(badString), BAD.toString(), "test val: " + badString);
    }

    @Test
    public void testBadWithVowelsQuestionConsonantsQuestionVowels() {
        String badString = "aadfedeo?fgh?ion";

        Assert.assertEquals(classifyStrings.classifyStrings(badString), BAD.toString(), "test val: " + badString);
    }

    // MIXED

    @Test
    public void testMixedWithVowels() {
        String mixedString = "aa?dfedeofghhion";

        Assert.assertEquals(classifyStrings.classifyStrings(mixedString), MIXED.toString(), "test val: " + mixedString);
    }

    @Test
    public void testMixedWithConsonants() {
        String mixedString = "aag?dfdedeofghhion";

        Assert.assertEquals(classifyStrings.classifyStrings(mixedString), MIXED.toString(), "test val: " + mixedString);
    }

    @Test
    public void testMixedWithVowelSurroundedByQuestions() {
        String mixedString = "aag?a?dfdfedeofghhion";

        Assert.assertEquals(classifyStrings.classifyStrings(mixedString), MIXED.toString(), "test val: " + mixedString);
    }

    @Test
    public void testMixedWithDoubleByQuestion() {
        String mixedString = "aag?aa??dfdfedeofghhion";

        Assert.assertEquals(classifyStrings.classifyStrings(mixedString), MIXED.toString(), "test val: " + mixedString);
    }

    @Test
    public void testMixedStartsByQuestionAndConsonants() {
        String mixedString = "?hhhgaadfdfedeofghhionnnn";

        Assert.assertEquals(classifyStrings.classifyStrings(mixedString), MIXED.toString(), "test val: " + mixedString);
    }
    @Test
    public void testMixedStartsByQuestionAndVowels() {
        String mixedString = "?aadfdfedeofghhionnnn";

        Assert.assertEquals(classifyStrings.classifyStrings(mixedString), MIXED.toString(), "test val: " + mixedString);
    }

    @Test
    public void testMixedEndsByConsonantsAndQuestion() {
        String mixedString = "aagaadfdfedeofghhionnnn?";

        Assert.assertEquals(classifyStrings.classifyStrings(mixedString), MIXED.toString(), "test val: " + mixedString);
    }

    @Test
    public void testMixedEndsByLowelsAndQuestion() {
        String mixedString = "aagaadfdfedeofghhio?";

        Assert.assertEquals(classifyStrings.classifyStrings(mixedString), MIXED.toString(), "test val: " + mixedString);
    }

    @Test
    public void testMixedOnlyQuestions() {
        String mixedString = "????????";

        Assert.assertEquals(classifyStrings.classifyStrings(mixedString), MIXED.toString(), "test val: " + mixedString);
    }

    // many ?
    // "df?aa?df" - MIXED
    // "df?aa???df" - MIXED

}
