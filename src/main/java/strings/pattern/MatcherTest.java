package strings.pattern;

import org.testng.Assert;
import org.testng.annotations.Test;

public class MatcherTest {

    Matcher matcher = new Matcher();

    /*@Test
    public void testJustBeginning() {
        String pattern = "^";
    }
    @Test
    public void testJustEnding() {
        String pattern = "$";
    }*/

    @Test
    public void testMatchBeginning() {
        String pattern = "^abc";
        String toMatch = "abcde";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), true);
    }

    @Test
    public void testNotMatchBeginning() {
        String pattern = "^abc";
        String toMatch = "abdde";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), false);
    }

    @Test
    public void testMatchEnding() {
        String pattern = "abc$";
        String toMatch = "sdabcdeabc";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), true);
    }

    @Test
    public void testNotMatchEnding() {
        String pattern = "abc$";
        String toMatch = "ddebc";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), false);
    }

    @Test
    public void testMatchSubstring() {
        String pattern = "abc";
        String toMatch = "ddeabcsdfsss";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), true);
    }

    @Test
    public void testNotMatchSubstring() {
        String pattern = "abc";
        String toMatch = "ddeacsdfsss";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), false);
    }


    // Full Match
    @Test
    public void testMatchFull() {
        String pattern = "^ddeabcsdfsss$";
        String toMatch = "ddeabcsdfsss";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), true);
    }

    @Test
    public void testNotMatchFull() {
        String pattern = "^ddeabcsdfsss$";
        String toMatch = "ddeabcsdfssz";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), false);
    }

    // Single word

    @Test
    public void testMatchSingleSymbol() {
        String pattern = "c";
        String toMatch = "abacaba";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), true);
    }

    @Test
    public void testNotMatchSingleSymbol() {
        String pattern = "c";
        String toMatch = "abazaba";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), false);
    }

    @Test
    public void testPatternLongerThanLine() {
        String pattern = "abacabac";
        String toMatch = "abacaba";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), false);
    }

    @Test
    public void testLongestLine() {
        String pattern = "a".repeat((int)(Math.pow(10, 5)/2)) + "b";
        String toMatch = "a".repeat(-1 + (int)Math.pow(10, 5)) + "b";

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), true);
    }

    @Test
    public void testLongestLineAndPattern() {
        String pattern = "a".repeat((int)Math.pow(10, 5));
        String toMatch = pattern;

        Assert.assertEquals(matcher.regexMatching(pattern, toMatch), true);
    }

}
