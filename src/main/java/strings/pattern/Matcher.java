package strings.pattern;

public class Matcher {

    /** 1 ≤ test.length ≤ 10^5
     * @param pattern - ^ OR $ symbols and string to match
     * @param test - string to check
     * @return
     */
    // region Still not working good enough for codesignal
    boolean regexMatching(String pattern, String test) {
        char[] charsOfPattern = pattern.toCharArray();
        char[] charsOfTestLine = test.toCharArray();

        if (charsOfPattern[0] == "^".charAt(0) && charsOfPattern[charsOfPattern.length-1] == "$".charAt(0)) {
            if (charsOfPattern.length - 2 != charsOfTestLine.length) {
                return false;
            }
            for (int i = 1; i < charsOfPattern.length-1; i++) {
                if (charsOfTestLine[i-1] != charsOfPattern[i]) {
                    return false;
                }
            }
            return true;
        } else if (charsOfPattern[0] == "^".charAt(0)) {
            if (charsOfPattern.length - 1 > charsOfTestLine.length) {
                return false;
            }
            for (int i = 1; i < charsOfPattern.length; i++) {
                if (charsOfPattern[i] != charsOfTestLine[i-1]) {
                    return false;
                }
            }
            return true;
        } else if (charsOfPattern[charsOfPattern.length-1] == "$".charAt(0)) {
            if (charsOfPattern.length - 1 > charsOfTestLine.length) {
                return false;
            }
            for (int i = 0; i < charsOfPattern.length-1; i++) {
                if (charsOfPattern[i] != charsOfTestLine[charsOfTestLine.length - charsOfPattern.length + i + 1]) {
                    return false;
                }
            }
            return true;
        } else  {
            if (charsOfPattern.length > charsOfTestLine.length) {
                return false;
            }
            for (int i = 0; i < charsOfTestLine.length; i++) {
                if (charsOfTestLine[i] == charsOfPattern[0]) {
                    if (charsOfPattern.length == 1) {
                        return true;
                    }
                    for (int j = charsOfPattern.length - 1; j > 0; j--) {
                        if (charsOfTestLine[i+j] != charsOfPattern[j]) {
                            break;
                        }
                        if (j == 1) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    // endregion

}
